[![pipeline status](https://gitlab.com/mfkuntz/deck-of-cards/badges/master/pipeline.svg)](https://gitlab.com/mfkuntz/deck-of-cards/commits/master)

# Deck of Cards

A deck of a cards is a simple collection (Deck) of type Card. 

## Deck

* `shuffle` - modifies the underlying collection, randomly re-ordering the cards. 
* `dealOnCard` - removes the first card from the deck. Throws if the collection is empty
* `tryDeal` - removes the first card from the deck. Returns null if the collection is empty

## Card

* Suit - Enum type of `CLUBS, DIAMONDS, HEARTS, SPADES`
* Rank - Enum type representing the face value of the card, from 2 through King and Ace

# Use

### Test

Run unit tests with the Gradle Wrapper

`./gradlew test`

### Sample Application

Run a very basic application that deals out an entire deck to STDOUT 

`./gradlew run`