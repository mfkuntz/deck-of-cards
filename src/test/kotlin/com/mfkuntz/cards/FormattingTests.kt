package com.mfkuntz.cards

import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test

class FormattingTests {

    @Test
    fun onTitleCaseEmptyString_shouldReturnEmptyString() {
        val expected = ""
        val result = expected.toTitleCase()
        expected shouldEqual result
    }
    @Test
    fun onTitleCase1LongString_shouldReturnValidString() {
        val expected = "a"
        val result = expected.toTitleCase()
        "A" shouldEqual result
    }

    @Test
    fun onTitleCaseCapitalString_shouldReturnValidString() {
        val result = "CAT".toTitleCase()
        "Cat" shouldEqual result
    }
    @Test
    fun onTitleCaseLowerString_shouldReturnValidString() {
        val result = "cat".toTitleCase()
        "Cat" shouldEqual result
    }
    @Test
    fun onTitleCaseCardSuit_shouldReturnValidString() {
        val result = Suit.HEARTS.name.toTitleCase()
        "Hearts" shouldEqual result
    }


}