package com.mfkuntz.cards

import org.amshove.kluent.shouldEqual
import org.amshove.kluent.shouldThrow
import org.junit.jupiter.api.Test
import java.util.*

class DeckTests {

    @Test
    fun onInitialization_countShouldBe52() {
        val deck = Deck()
        val collection = deck.getUnderlyingDeckCollection()
        collection.size shouldEqual 52
    }

    // region Shuffle

    // since we control the random seed, we can correctly assert cards


    @Test
    fun onShuffle_firstElementShouldBeKnown() {
        val deck = Deck(TestRandomSource())
        deck.shuffle()

        deck.dealOneCard() shouldEqual Card(Suit.CLUBS, Rank.SIX)
    }
    @Test
    fun onShuffle_lastElementShouldBeKnown() {
        val deck = Deck(TestRandomSource())
        deck.shuffle()
        val collection = deck.getUnderlyingDeckCollection()

        collection.last() shouldEqual Card(Suit.HEARTS, Rank.TWO)
    }
    // endregion

    // region Deal
    @Test
    fun onDealOnce_collectionSizeShouldBe51() {
        val deck = Deck()
        deck.dealOneCard()

        val collection = deck.getUnderlyingDeckCollection()
        collection.size shouldEqual 51
    }

    @Test
    fun onDeal52Times_collectionShouldBeEmpty() {
        val deck = Deck()

        for (i in 0..51) {
            deck.dealOneCard()
        }

        val collection = deck.getUnderlyingDeckCollection()
        collection.size shouldEqual 0
    }

    @Test
    fun onDeal53Times_shouldThrowIndexOutOfBounds() {
        val deck = Deck()

        // get to empty state
        for (i in 0..51) {
            deck.dealOneCard()
        }
        // next access will throw
        val throws = { deck.dealOneCard() }

        throws shouldThrow IndexOutOfBoundsException::class
    }
    // endregion

    // region tryDeal
    @Test
    fun onTryDealOnce_collectionSizeShouldBe51() {
        val deck = Deck()
        deck.tryDeal()

        val collection = deck.getUnderlyingDeckCollection()
        collection.size shouldEqual 51
    }
    @Test
    fun onTryDeal52Times_collectionShouldBeEmpty() {
        val deck = Deck()

        for (i in 0..51) {
            deck.tryDeal()
        }

        val collection = deck.getUnderlyingDeckCollection()
        collection.size shouldEqual 0
    }
    @Test
    fun onTryDeal53Times_shouldBeNull() {
        val deck = Deck()

        // get to empty state
        for (i in 0..51) {
            deck.tryDeal()
        }
        // next access will throw
        val card = deck.tryDeal()
        card shouldEqual null
    }
    // endregion

}

/**
 * Fake random for predictable unit tests
 */
class TestRandomSource : IRandomSource {
    private val random = Random(42)

    override fun nextInt(upperBound: Int): Int = random.nextInt(upperBound)
}