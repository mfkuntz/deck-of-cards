package com.mfkuntz.cards

/**
 * converts a String to TitleCase, by capitalizing the first letter and lower-casing the rest
 */
fun String.toTitleCase() : String {
    if (this.isEmpty()) {
        return this
    }
    return this[0].toUpperCase() + this.substring(1).toLowerCase()
}