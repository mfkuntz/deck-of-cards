package com.mfkuntz.cards

fun main(args: Array<String>) {
    Dealer().deal()
}

/**
 * A simple class demonstrating the Deck implementation by logging shuffled cards to STDOUT
 */
class Dealer {
    private val deck = Deck()
    init{
        deck.shuffle()
    }

    /**
     * print out the shuffled deck of cards
     */
    fun deal() {
        do {
            val card = deck.tryDeal()
            card ?: break
            printCard(card)
        } while (card != null)
    }

    private fun printCard(card: Card) {
        System.out.println("${card.rank.name.toTitleCase()} of ${card.suit.name.toTitleCase()}")
    }
}