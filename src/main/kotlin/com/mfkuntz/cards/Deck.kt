package com.mfkuntz.cards

import org.jetbrains.annotations.TestOnly

/**
 * A collection of 52 Cards with functions for playing cards.
 */
class Deck(private val random: IRandomSource = RandomSource()) {

    private val deck: MutableList<Card>


    init {
        deck = initializeDeck().toMutableList()
    }

    /**
     * Randomly shuffles the deck collection using the modern Fisher–Yates shuffle
     */
    fun shuffle() {
        val upperBound = deck.size - 1

        for(i in upperBound downTo 1) {
            // random number between 0 -> rand -> i
            val rand = random.nextInt(i)

            // swap i and rand
            val tmp = deck[rand]
            deck[rand] = deck[i]
            deck[i] = tmp
        }
    }

    /**
     * test note - the dealOneCard spec was vague enough for "no card is dealt" that I decided to go with throwing
     * an exception like a normal collection would behave. For actual application use, I imagine using tryDeal in most
     * cases would be the better way.
     */

    /**
     * pops the first card off of the stack, removing it from the Deck
     * @throws IndexOutOfBoundsException when the collection is empty
     * @return the first Card
     */
    fun dealOneCard() : Card = deck.removeAt(0)

    /**
     * if there are more cards in the Deck, deal one. Otherwise, return null
     */
    fun tryDeal(): Card? {
        return if (deck.size > 0) {
            dealOneCard()
        } else {
            null
        }
    }


    /**
     * creates a new list of 52 cards ordered by Suit, Rank
     */
    private fun initializeDeck() : List<Card> {
        return Suit.values()
                .flatMap { suit ->
                    // for each possible Suit, map all possible Ranks
                    Rank.values()
                        .map { rank ->
                            Card(suit, rank)
                        }
                }
    }


    /**
     * gets the underlying desk collection object for testing purposes
     */
    @TestOnly
    internal fun getUnderlyingDeckCollection() : MutableList<Card> = deck
}