package com.mfkuntz.cards

import java.security.SecureRandom

interface IRandomSource {
    fun nextInt(upperBound: Int): Int
}

class RandomSource : IRandomSource {

    // use secure random instead of Random, since this could end up in a casino!
    private val random = SecureRandom()

    override fun nextInt(upperBound: Int): Int = random.nextInt(upperBound)
}